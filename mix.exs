defmodule AlbertoAmqpClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :alberto_amqp_client,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [mod: {AlbertoAmqpClient, []}, extra_applications: [:logger]]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:amqp, "~> 3.3.0"},
      {:httpoison, "~> 2.0"},
      {:jason, "~> 1.4"},
      {:ex_rabbit_pool, git: "https://github.com/haroldvera/ex_rabbit_pool", branch: "master"},
      {:monad, git: "https://github.com/haroldvera/monad", branch: "develop"}
    ]
  end
end
