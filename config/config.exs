import Config

config :alberto_amqp_client,
  pools: [
    [
      name: {:local, :producers_pool},
      worker_module: ExRabbitPool.Worker.RabbitConnection,
      size: 5,
      max_overflow: 0
    ],
    [
      name: {:local, :consumers_pool},
      worker_module: ExRabbitPool.Worker.RabbitConnection,
      size: 5,
      max_overflow: 0
    ]
  ]

config :alberto_amqp_client,
  queues: []

import_config "#{config_env()}.exs"
