defmodule AlbertoAmqpClient do
  @moduledoc """
  Documentation for AlbertoAmqpClient.
  """
  use Application
  require Logger
  # @app "alberto_amqp_client"
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      #  %{
      #   id: ExRabbitPool.PoolSupervisor,
      #  start:
      #     {ExRabbitPool.PoolSupervisor, :start_link,
      #     [
      #        [
      #             rabbitmq_config: AlbertoAmqp.Config.get(),
      #         connection_pools: Application.get_env(:alberto_amqp_client, :pools)
      #       ]
      #     ]}
      # }
    ]

    # sfdddfsdf
    opts = [strategy: :one_for_one, name: AlbertoAmqp.Application.Supervisor]
    s = Supervisor.start_link(children, opts)
    # AlbertoAmqp.Client.initialize()
    s
  end
end

defmodule AlbertoAmqp.Config do
  def get(),
    do: [
      channels: 5,
      host: System.get_env("AMQP_HOST"),
      port: System.get_env("AMQP_PORT") |> String.to_integer(),
      username: System.get_env("AMQP_USERNAME"),
      password: System.get_env("AMQP_PASSWORD")
    ]
end

defmodule AlbertoAmqp.Client do
  use AMQP
  require Monad.Error, as: Error

  def initialize(),
    do:
      Application.get_env(:alberto_amqp_client, :queues)
      |> Enum.map(&AlbertoAmqp.Client.AlbertoQueue.init(struct(&1.module, &1.config)))

  def amqp_declare_exchange(channel, exchange, exchange_type, opts) when exchange_type == "direct", do:
    amqp_declare_exchange_direct(channel, exchange, opts)
  def amqp_declare_exchange(channel, exchange, _exchange_type, opts), do:
    amqp_declare_exchange_fanout(channel, exchange, opts)

  def amqp_declare_exchange_fanout(channel, exchange, opts) do
    case Exchange.fanout(channel, exchange, opts) do
      :ok -> Error.return(:ok)
      _ -> Error.fail("no se ha podido crear el exchange #{exchange}")
    end
  end

  def amqp_declare_exchange_direct(channel, exchange, opts) do
    case Exchange.direct(channel, exchange, opts) do
      :ok -> Error.return(:ok)
      _ -> Error.fail("no se ha podido crear el exchange #{exchange}")
    end
  end

  def amqp_queue_bind(channel, queue, exchange, routing_key) do
    case Queue.bind(channel, queue, exchange, routing_key: routing_key) do
      :ok ->
        Error.return(:ok)

      _ ->
        Error.fail(
          "no se ha podido crear el binding entre el exchange #{exchange} y el queue #{queue}"
        )
    end
  end

  def amqp_queue_bind(channel, queue, exchange) do
    case Queue.bind(channel, queue, exchange) do
      :ok ->
        Error.return(:ok)

      _ ->
        Error.fail(
          "no se ha podido crear el binding entre el exchange #{exchange} y el queue #{queue}"
        )
    end
  end

  defprotocol AlbertoQueue do
    def init(a)
  end

  defmodule WorkflowDefault do
    defstruct queue: "", exchange: "", queue_error: "", queue_arguments: []
  end

  defmodule WorkflowNewDefault do
    defstruct queue: "", exchange: "", queue_error: "", queue_arguments: []
  end

  defmodule ListenerDefault do
    defstruct queue: "", exchange: "", routing_key: "", queue_error: "", queue_arguments: []
  end

  defimpl AlbertoAmqp.Client.AlbertoQueue, for: AlbertoAmqp.Client.WorkflowDefault do
    def init(a),
      do:
        AlbertoAmqp.Client.init(%{
          queue: a.queue,
          exchange: a.exchange,
          queue_error: a.queue_error,
          queue_arguments: a.queue_arguments
        })
  end

  defimpl AlbertoAmqp.Client.AlbertoQueue, for: AlbertoAmqp.Client.WorkflowNewDefault do
    def init(a),
      do:
        AlbertoAmqp.Client.init(%{
          queue: a.queue,
          exchange: a.exchange,
          queue_error: a.queue_error,
          queue_arguments: a.queue_arguments
        })
  end

  defimpl AlbertoAmqp.Client.AlbertoQueue, for: AlbertoAmqp.Client.ListenerDefault do
    def init(a),
      do:
        AlbertoAmqp.Client.init(%{
          queue: a.queue,
          exchange: a.exchange,
          routing_key: a.routing_key,
          queue_error: a.queue_error,
          queue_arguments: a.queue_arguments
        })
  end


  def init(%{
    queue: queue,
    exchange: exchange,
    routing_key: routing_key,
    queue_error: queue_error,
    queue_arguments: queue_arguments
  }) do
    ExRabbitPool.with_channel(:producers_pool, fn {:ok, channel} ->
      Error.m do
        _error_queue <- Queue.declare(channel, queue_error, durable: true)
        _exchange <- amqp_declare_exchange(channel, exchange, "direct", durable: true)

        _queue <-
          Queue.declare(
            channel,
            queue,
            durable: true,
            arguments: queue_arguments
          )

        # [
        # {"x-dead-letter-exchange", :longstr, ""},
        # {"x-dead-letter-routing-key", :longstr, "#{queue}_error"}
        # ]
        amqp_queue_bind(channel, queue, exchange, routing_key)
      end
    end)
  end
  def init(%{
        queue: queue,
        exchange: exchange,
        queue_error: queue_error,
        queue_arguments: queue_arguments
      }) do
    ExRabbitPool.with_channel(:producers_pool, fn {:ok, channel} ->
      Error.m do
        _error_queue <- Queue.declare(channel, queue_error, durable: true)
        _exchange <- amqp_declare_exchange_fanout(channel, exchange, durable: true)

        _queue <-
          Queue.declare(
            channel,
            queue,
            durable: true,
            arguments: queue_arguments
          )

        # [
        # {"x-dead-letter-exchange", :longstr, ""},
        # {"x-dead-letter-routing-key", :longstr, "#{queue}_error"}
        # ]
        amqp_queue_bind(channel, queue, exchange)
      end
    end)
  end

  def send(exchange, message),
    do:
      ExRabbitPool.with_channel(:producers_pool, fn {:ok, channel} ->
        case AMQP.Basic.publish(channel, exchange, "", message) do
          :ok -> Error.return(message)
          _ -> Error.fail("error enviando el mensage a rabbit #{inspect(message)}")
        end
      end)

  def amqp_ok_or_reject({:ok, _} = a, adapter, channel, delivery_tag) do
    case adapter.ack(channel, delivery_tag, requeue: false) do
      :ok ->
        a

      e ->
        Error.return(
          "Problemas en rabbitmq al hacer ACK rabbit(#{inspect(e)}) con el siguiene mensaje (#{
            inspect(a)
          })"
        )
    end
  end

  def amqp_ok_or_reject(a, adapter, channel, delivery_tag) do
    case adapter.reject(channel, delivery_tag, requeue: false) do
      :ok ->
        a

      e ->
        Error.return(
          "Problemas en rabbitmq al hacer REJECT rabbit(#{inspect(e)}) con el siguiene mensaje (#{
            inspect(a)
          })"
        )
    end
  end
end
