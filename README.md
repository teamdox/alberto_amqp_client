# AlbertoAmqpClient

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `alberto_amqp_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:alberto_amqp_client, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/alberto_amqp_client](https://hexdocs.pm/alberto_amqp_client).

